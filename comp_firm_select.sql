USE labor_SQL;

SELECT maker m, type t
FROM product
ORDER BY m ASC;

SELECT model m, ram r, screen s, price p
FROM laptop
ORDER BY r ASC, p DESC;

SELECT * FROM printer p
WHERE p.color = 'y'
ORDER BY p.price DESC;

SELECT model, speed, hd, cd, price 
FROM pc
WHERE price < 600.00 AND (cd = '12x' OR cd =  '24x');

SELECT * FROM pc 
WHERE speed >= 500 AND price < 800.00
ORDER BY price DESC;

SELECT * FROM printer
WHERE type NOT LIKE 'Matrix' AND price < 300.00;

SELECT model, speed 
FROM pc p 
WHERE p.price IN(400.00, 600.00)
ORDER BY p.hd;

SELECT model, speed, hd 
FROM pc p
WHERE (p.hd = 10 OR p.hd = 20) 
AND p.model IN (SELECT model FROM product WHERE maker = 'A')
ORDER BY p.speed ASC;

SELECT model m, speed s, hd h, price p 
FROM laptop
WHERE screen >= 12
ORDER BY p DESC;

SELECT model FROM pc
WHERE model RLIKE '.*1.*1.*';

SELECT pr.maker, pr.type, pc.speed, pc.hd
FROM product pr
RIGHT JOIN pc USING (model)
WHERE pc.hd <= 8;

SELECT pr.maker
FROM product pr
RIGHT JOIN pc USING (model)
WHERE pc.speed >= 600;

SELECT pr.maker
FROM product pr
RIGHT JOIN laptop l USING (model)
WHERE l.speed <= 500;

SELECT DISTINCT l.model m, laptop.model, l.hd, l.ram
FROM laptop l
RIGHT JOIN laptop USING (hd, ram)
ORDER BY m ASC;






