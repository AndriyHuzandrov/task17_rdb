SELECT maker FROM product
WHERE product.model IN (SELECT model FROM pc);

#?
SELECT maker FROM product
WHERE product.model <> ALL (SELECT model FROM laptop) 
AND product.model <> ALL (SELECT model FROM printer); 

SELECT maker FROM product
WHERE product.model = ANY (SELECT model FROM pc); 

#?
SELECT maker FROM product p
WHERE p.model IN (SELECT model FROM laptop)
AND p.model IN (SELECT model FROM printer);
