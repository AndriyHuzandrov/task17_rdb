SELECT trip_no AS 'Flight', town_from AS 'From', town_to AS 'To' 
FROM trip t
WHERE town_from LIKE 'London' OR town_to LIKE 'London'
ORDER BY t.time_out ASC;

SELECT trip_no, town_from, town_to, plane
FROM trip t
WHERE plane LIKE 'TU-134'
ORDER BY t.time_out;

SELECT trip_no, town_from, town_to, plane
FROM trip t
WHERE plane <> 'IL-86'
ORDER BY plane ASC;

SELECT trip_no, town_from, town_to
FROM trip
WHERE town_to <> 'Rostov' AND town_from <> 'Rostov' 
ORDER BY plane ASC;

SELECT trip_no, time_out
FROM trip
WHERE HOUR(time_out) BETWEEN 12 AND 17
ORDER BY time_out ASC;

SELECT trip_no, time_in
FROM trip
WHERE HOUR(time_in) BETWEEN 17 AND 23
ORDER BY time_in ASC;

SELECT date FROM pass_in_trip
WHERE place LIKE '1_'
ORDER BY date;

SELECT date FROM pass_in_trip
WHERE place LIKE '_c'
ORDER BY date;

SELECT SUBSTRING_INDEX(name, ' ', -1) Surname
FROM passenger
WHERE SUBSTRING_INDEX(name, ' ', -1) RLIKE '^C.+';

SELECT SUBSTRING_INDEX(name, ' ', -1) Surname
FROM passenger
WHERE SUBSTRING_INDEX(name, ' ', -1) RLIKE '^[^J].+'
ORDER BY Surname;


