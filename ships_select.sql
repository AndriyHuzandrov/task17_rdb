SELECT name n, class c
FROM ships
ORDER BY n ASC;

SELECT class AS 'Ship class'
FROM classes
WHERE country LIKE 'Japan';

SELECT name, launched
FROM ships
WHERE launched >= 1920 AND launched <= 1942
ORDER BY launched;

SELECT ship FROM outcomes o
WHERE o.battle LIKE 'Guadalcanal' AND o.result NOT LIKE 'sunk'
ORDER BY o.ship DESC;

SELECT ship, battle, result 
FROM outcomes
WHERE result LIKE 'sunk'
ORDER BY ship DESC;

SELECT class, displacement
FROM classes c
WHERE c.displacement >= 40
ORDER BY c.type;

SELECT name FROM ships
WHERE name RLIKE '^W.*n$';

SELECT name FROM ships
WHERE name RLIKE '.*e.*e.*';

SELECT name, launched
FROM ships
WHERE name NOT RLIKE '.*a$';

SELECT name FROM battles
WHERE name RLIKE ' .+[^c]$';

SELECT country, class
FROM classes c
WHERE c.type IN (SELECT type FROM classes WHERE type = 'bb')
OR c.type IN (SELECT type FROM classes WHERE type = 'bc');


