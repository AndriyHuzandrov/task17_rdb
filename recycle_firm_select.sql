SELECT point, DATE_FORMAT(o.date, '%d %M %y') AS 'DATE', o.out
FROM outcome_o o
WHERE o.out > 2000.00
ORDER BY date DESC;

SELECT inc AS 'MONEY', DATE_FORMAT(i.date, '%d %M %y') AS 'DATE'
FROM income i
WHERE point = 1
ORDER BY i.inc ASC;

SELECT inc AS 'MONEY', DATE_FORMAT(i.date, '%d %M %y') AS 'DATE'
FROM income_o i 
WHERE i.inc IN(5000.00, 10000.00)
ORDER BY i.inc ASC;

SELECT inc AS 'MONEY' 
FROM income
WHERE point = 1
ORDER BY inc;

SELECT o.out AS 'MONEY' 
FROM outcome o
WHERE point = 2
ORDER BY o.out ASC;

SELECT * FROM outcome o
WHERE MONTH(date) = 3;

SELECT * FROM outcome_o o 
WHERE DAYOFMONTH(date) = 14;
